//Задание 1
//50, 100, 200, 500, 1000
function change(money) {
    let c = [50, 100, 200, 500, 1000]
    let r = 0
    let n = money;
    for (let i = 0; i < c.length; c++) {
        r += n // i
        n %= i
    }
    return r;
}

//Задание 2


function anagramTest(s1, s2) {

    if (!s1 || !s2 || s1.length !== s2.length) {
        return false;
    }
    let lS1 = s1.toLowerCase();
    let lS2 = s2.toLowerCase();

    if (lS1 === lS2) {
        return false;
    }

    let rS1 = lS1.split('').sort().join('');
    let rS2 = lS2.split('').sort().join('');

    return rS1 === rS2;
}


//Задание 3
function zero(arr) {
    let count = 0;
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] === 0) {
            arr.splice(i, 1);
            count++;
            i--;
        }
    }
    for (let i = 0; i < count; i++) {
        arr.push(0);
    }
    return arr;
}